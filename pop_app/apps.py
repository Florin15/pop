from django.apps import AppConfig


class PopAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pop_app'
